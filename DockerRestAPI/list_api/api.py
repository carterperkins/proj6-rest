# Laptop Service

from flask import Flask, make_response, request
from flask_restful import Resource, Api
from pymongo import MongoClient
import os
from datetime import datetime

# Instantiate the app
app = Flask(__name__)
api = Api(app)

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb

def getTimes(k):
    _items = db.tododb.find()
    items = [item for item in _items]
    open_times = []
    for i in items:
        if k in i.keys():
            open_times.append(i[k])
    return open_times
    
def getCSVFull(o, c):
        data_raw = o + c
        data_list = []
        for i in range(len(o)):
            data_list.append(data_raw[i])
            data_list.append(data_raw[i + len(o)])
        data = "Open Time,Close Time\n"
        for i in range(0, len(data_list), 2):
            data += data_list[i] + "," + data_list[i+1] + "\n"
        return data
        
def getCSV(k, t):
        data_list = k
        data = "{} Time\n".format(t)
        for i in range(len(k)):
            data += data_list[i] + "\n"
        return data


class ListAll(Resource):
    def get(self):
        # return json of open and close times
        return {'Open': getTimes('open'), 'Close': getTimes('close')}
        
class ListOpenOnly(Resource):
    def get(self):
        return {'Open': getTimes('open')}

class ListCloseOnly(Resource):
    def get(self):
        return {'Close': getTimes('close')}
        
class ListAllFormat(Resource):
    def get(self, format):
        t = request.args.get('t')
        app.logger.debug("T: {}".format(t))
        if format == 'json':
            o = getTimes('open')
            c = getTimes('close')
            if t and int(t) > 0 and int(t) < len(o): # count must be valid 
                app.logger.debug('here')
                o = sorted(o, key=lambda x: datetime.strptime(x, '%a %m/%d %H:%M'))
                c = sorted(c, key=lambda x: datetime.strptime(x, '%a %m/%d %H:%M'))
                t = int(t)
                o = o[:t]
                c = c[:t]
                app.logger.debug(o)
                app.logger.debug(c)
            return {'Open': o, 'Close': c}
        if format == 'csv':
            o = getTimes('open')
            c = getTimes('close')
            if t and int(t) > 0 and int(t) < len(o): # count must be valid 
                app.logger.debug('here')
                o = sorted(o, key=lambda x: datetime.strptime(x, '%a %m/%d %H:%M'))
                c = sorted(c, key=lambda x: datetime.strptime(x, '%a %m/%d %H:%M'))
                app.logger.debug("o: {}\nc: {}".format(o, c))
                t = int(t)
                o = o[:t]
                c = c[:t]
                app.logger.debug(o)
                app.logger.debug(c)
            resp = make_response(getCSVFull(o, c))
            resp.headers['content-type'] = 'application/csv'
            return resp
        return 'Invalid Resource'

class ListOpenOnlyFormat(Resource):
    def get(self, format):
        o = getTimes('open')
        t = request.args.get('t')
        if t and int(t) > 0 and int(t) < len(o): # count must be valid 
            o = sorted(o, key=lambda x: datetime.strptime(x, '%a %m/%d %H:%M'))
            t = int(t)
            o = o[:t]
            app.logger.debug(o)
        if format == 'json':
            return {'Open': o}
        if format == 'csv':
            resp = make_response(getCSV(o, 'Open'))
            resp.headers['content-type'] = 'application/csv'
            return resp
        return 'Invalid Resource'
        
class ListCloseOnlyFormat(Resource):
    def get(self, format):
        c = getTimes('close')
        t = request.args.get('t')
        if t and int(t) > 0 and int(t) < len(c): # count must be valid 
            c = sorted(c, key=lambda x: datetime.strptime(x, '%a %m/%d %H:%M'))
            t = int(t)
            c = c[:t]
        if format == 'json':
            return {'Close': c}
        if format == 'csv':
            resp = make_response(getCSV(c, 'Close'))
            resp.headers['content-type'] = 'application/csv'
            return resp
        return 'Invalid Resource'
            
# Create routes
# Another way, without decorators
api.add_resource(ListAll, '/listAll')
api.add_resource(ListOpenOnly, '/listOpenOnly')
api.add_resource(ListCloseOnly, '/listCloseOnly')

api.add_resource(ListAllFormat, '/listAll/<format>')
api.add_resource(ListOpenOnlyFormat, '/listOpenOnly/<format>')
api.add_resource(ListCloseOnlyFormat, '/listCloseOnly/<format>')


# Run the application
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)
