<html>
    <head>
        <title>ACP Brevet Control (Consumer)</title>
    </head>

    <body>
        <h1>ACP Brevet Control (Consumer)</h1>
        <ul>
            <?php
            echo "<h1>/listAll API Usage</h1>";
            $json = file_get_contents('http://list-service/listAll');
            $obj = json_decode($json);
            $open = $obj->Open;
            $close = $obj->Close;
            echo "<h3>Open:</h3>";  
            foreach ($open as $o) {
                echo "<li>$o</li>";
            }
            echo "<h3>Close:</h3>";
            foreach ($close as $c) {
                echo "<li>$c</li>";
            }
            echo "<br><br><br>";
            echo "<h1>/listOpenOnly API Usage</h1>";
            $json = file_get_contents('http://list-service/listOpenOnly');
            $obj = json_decode($json);
            $open = $obj->Open;
            echo "<h3>Open:</h3>";  
            foreach ($open as $o) {
                echo "<li>$o</li>";
            }
            echo "<br><br><br>";
            echo "<h1>/listCloseOnly API Usage</h1>";
            $json = file_get_contents('http://list-service/listCloseOnly');
            $obj = json_decode($json);
            $close = $obj->Close;
            echo "<h3>Close:</h3>";  
            foreach ($close as $c) {
                echo "<li>$c</li>";
            }
            echo "<br><br><br>";
            echo "<h1>/listOpenOnly Top 2 API Usage</h1>";
            $json = file_get_contents('http://list-service/listOpenOnly/json?t=2');
            $obj = json_decode($json);
            $open = $obj->Open;
            echo "<h3>Open:</h3>";  
            foreach ($open as $o) {
                echo "<li>$o</li>";
            }
            echo "<br><br><br>";
            echo "<h1>/listCloseOnly Top 2 API Usage</h1>";
            $json = file_get_contents('http://list-service/listCloseOnly/json?t=2');
            $obj = json_decode($json);
            $close = $obj->Close;
            echo "<h3>Close:</h3>";  
            foreach ($close as $c) {
                echo "<li>$c</li>";
            }
            echo "<br><br><br>";
            ?>

        </ul>
    </body>
</html>
