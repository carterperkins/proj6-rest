import os
from flask import Flask, redirect, url_for, request, render_template
import flask
from pymongo import MongoClient
import arrow
import acp_times
import urllib.parse
import json

app = Flask(__name__)

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb

@app.route('/')
def todo():
    _items = db.tododb.find()
    items = [item for item in _items]
    print(items)
    return render_template('todo.html', items=items)
    
@app.route("/_clear", methods=['POST'])
def clear():
    db.tododb.drop()
    app.logger.debug("DROP TABLE: {}".format(db.tododb.count()))
    return redirect(url_for('todo'))

@app.route("/display", methods=['POST'])
def display():
    _items = db.tododb.find()
    items = [item for item in _items]
    app.logger.debug("DB: {}".format(items))

    return render_template('display.html', items=items) #header=header, items=items)

@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request\n\n")
    app.logger.debug(request)
    app.logger.debug("\n\n")
    km = request.args.get('km', 999, type=float)
    distance = request.args.get('distance', default=None, type=int)
    begin_date = request.args.get('begin_date', default=None, type=str)
    begin_time = request.args.get('begin_time', default=None, type=str)
    start_time = arrow.get('{} {}'.format(begin_date, begin_time)).replace(tzinfo='US/Pacific').isoformat()
    
    app.logger.debug("km={} distance={}, begin_date={}, begin_time={}, start_time={}".format(km, distance, begin_date, begin_time, start_time))
    app.logger.debug("request.args: {}".format(request.args))
    
    open_time = acp_times.open_time(km, distance, start_time)
    close_time = acp_times.close_time(km, distance, start_time)

    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)


@app.route('/submit', methods=['POST', 'GET'])
def submit():
    
    if(db.tododb.count() != 0): # database isnt empty
        db.tododb.drop() # clear entries 
  # app.logger.debug("\n\n\n\n\n")
    data = urllib.parse.unquote(str(request.args.get('db')))
    app.logger.debug("\n\n_SUBMIT: {}\n\n".format(data))
   # app.logger.debug("data obj: {}".format( dataObj))
    src = u"[%s]" % data # format json properly
    data_dict = json.loads(src)
    for element in data_dict:
        app.logger.debug(element)
        db.tododb.insert_one(element)
  # checkpoint = {
   #     'checkpoint': request.form['location'],
   #     'miles': request.form['miles'],
   #     'km': request.form['km'],
   #     'open': request.form['open'],
   #     'close': request.form['close']
   # }'''
    #app.logger.debug("=====CHECKPOINT: {}".format(checkpoint))
    #db.tododb.insert_one(item_doc)
    return redirect(url_for('todo'))

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=80,debug=True)
